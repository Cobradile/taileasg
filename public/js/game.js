var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var bg;
var peice;
var opponent = 1;
var player = 0;
var firstPlayer = false;
var yourTurn = true;

var zones = [];
var pawns = [2];
var rooks = [2];
var knights = [2];
var bishops = [2];
var queens = [2];
var kings = [2];

var oldZone

var statusText
var gameOver = false;

const PIT = 30;

function preload ()
{
    this.load.image('board', 'assets/board.png');
    this.load.image('pawnB', 'assets/pawnB.png');
    this.load.image('rookB', 'assets/rookB.png');
    this.load.image('knightB', 'assets/knightB.png');
    this.load.image('bishopB', 'assets/bishopB.png');
    this.load.image('queenB', 'assets/queenB.png');
    this.load.image('kingB', 'assets/kingB.png');

    this.load.image('pawnW', 'assets/pawnW.png');
    this.load.image('rookW', 'assets/rookW.png');
    this.load.image('knightW', 'assets/knightW.png');
    this.load.image('bishopW', 'assets/bishopW.png');
    this.load.image('queenW', 'assets/queenW.png');
    this.load.image('kingW', 'assets/kingW.png');
}

function create ()
{    
    var self = this;
    this.socket = io();
    this.socket.on('currentPlayers', function (players) {
        Object.keys(players).forEach(function (id) {
          if (players[id].playerId === self.socket.id) {
            addPlayer(self, players[id]);
          }
        });
      });
    
    bg = this.add.image(100, 0, 'board').setOrigin(0, 0);
    var graphics = this.add.graphics().lineStyle(2, 0xffff00);

    for (let c = 0; c < 8; c++) 
    {
        zones[c] = [];
        for (let i = 0; i < 8; i++) {
            let zone = this.add
            .zone(bg.x + 55 + 70 * c, bg.y + 55 + 70 * i, 70, 70)
            .setRectangleDropZone(70, 70)
            .setOrigin(0, 0);

            zones[c][i] = zone;
        }
    }

    pawns[0] = [];
    for (let c = 0; c < 8; c++) 
    {
        let peice = this.add;
        peice = this.add.image(zones[c][1].x, zones[c][1].y, 'pawnW').setInteractive();
        peice.setScale(3);
        peice.x = zones[c][6].x;
        peice.y = zones[c][6].y;
        this.input.setDraggable(peice);
        pawns[0][c] = peice;
    }

    pawns[1] = [];
    for (let c = 0; c < 8; c++) 
    {
        let peice = this.add;
        peice = this.add.image(zones[c][6].x, zones[c][6].y, 'pawnB').setInteractive();
        peice.setScale(3);
        peice.x = zones[c][1].x;
        peice.y = zones[c][1].y;

        pawns[1][c] = peice;
        this.input.setDraggable(peice);
    }

    rooks[0] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'rookW').setInteractive();
        peice.setScale(3);
        rooks[0][c] = peice;
        this.input.setDraggable(peice);
    }
    rooks[1] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'rookB').setInteractive();
        peice.setScale(3);
        rooks[1][c] = peice;
        this.input.setDraggable(peice);
    }

    knights[0] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'knightW').setInteractive();
        peice.setScale(3);
        knights[0][c] = peice;
        this.input.setDraggable(peice);
    }
    knights[1] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'knightB').setInteractive();
        peice.setScale(3);
        knights[1][c] = peice;
        this.input.setDraggable(peice);
    }

    bishops[0] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'bishopW').setInteractive();
        peice.setScale(3);
        bishops[0][c] = peice;
        this.input.setDraggable(peice);
    }
    bishops[1] = [2];
    for (let c = 0; c < 2; c++) 
    {
        let peice = this.add;
        peice = this.add.image(1, 1, 'bishopB').setInteractive();
        peice.setScale(3);
        bishops[1][c] = peice;
        this.input.setDraggable(peice);
    }

    queens[1] = this.add.image(1, 1, 'queenB').setInteractive();
    queens[1].setScale(3);
    queens[0] = this.add.image(1, 1, 'queenW').setInteractive();
    queens[0].setScale(3);
    this.input.setDraggable(queens[0]);
    this.input.setDraggable(queens[1]);

    kings[1] = this.add.image(1, 1, 'kingB').setInteractive();
    kings[1].setScale(3);
    kings[0] = this.add.image(1, 1, 'kingW').setInteractive();
    kings[0].setScale(3);
    this.input.setDraggable(kings[0]);
    this.input.setDraggable(kings[1]);

    statusText = this.add.text(0, 0, 'Your Turn', { font: '64px Arial' }).setOrigin(0);
    statusText.setTint(0xff00ff, 0xffff00, 0x0000ff, 0xff0000);
    Phaser.Display.Align.In.Center(statusText, bg);
    statusText.visible = false;

    resetPos();
    showHide(false);
    // var container = this.add.container(400, 300, [ peice ]);
    // container.setSize(peice.width, peice.height);
    // container.setInteractive();            let peice = this.add;
    this.input.on('drag', function (pointer, gameObject, dragX, dragY) {
        gameObject.x = dragX;
        gameObject.y = dragY;
    });

    this.input.on('dragstart', function (pointer, gameObject) {
        oldZone = getSquare(gameObject);
        oldZone = zones[oldZone[0]][oldZone[1]];
        this.children.bringToTop(gameObject);
    }, this);

    this.input.on('dragenter', function (pointer, gameObject, dropZone) {
        //graphics.clear();
        //graphics.lineStyle(2, 0x00ffff);
        //graphics.strokeRect(zones[i, 0].x - zones[i, 0].input.hitArea.width / 2, zones[i, 0].y - zones[i, 0].input.hitArea.height / 2, zones[i, 0].input.hitArea.width, zones[i, 0].input.hitArea.height);
    });

    this.input.on('dragleave', function (pointer, gameObject, dropZone) {
        //graphics.clear();
        //graphics.lineStyle(2, 0xffff00);
        //graphics.strokeRect(zones[i, 0].x - zones[i, 0].input.hitArea.width / 2, zones[i, 0].y - zones[i, 0].input.hitArea.height / 2, zones[i, 0].input.hitArea.width, zones[i, 0].input.hitArea.height);
    });

    this.input.on('drop', function (pointer, gameObject, dropZone) {
        checkSquare(dropZone);
        gameObject.x = dropZone.x;
        gameObject.y = dropZone.y;

        let newZone = getSquare(gameObject);
        newZone = zones[newZone[0]][newZone[1]];

        self.socket.emit("placePeice", oldZone, newZone);
    });

    this.socket.on('placePeice', function (oldZone, newZone) {
        if (!gameOver)
        {
            if (!yourTurn)
            {
                var peice = getPeice(oldZone);
                checkSquare(newZone);
                peice.x = newZone.x;
                peice.y = newZone.y;
                yourTurn = true;
                text.visible = false;
            }
            else {
                yourTurn = false;
                self.children.bringToTop(statusText);
                statusText.setText('Waitin on Opponent');
                Phaser.Display.Align.In.Center(statusText, bg);
                statusText.visible = true;
            }
            console.log(yourTurn);
            setInput();
        }
        else
        {
            game_over();
        }
    });
    
    this.socket.on('isPlayerA', function () {
        player = 0;
        opponent = 1;
        firstPlayer = true;
        yourTurn = true;
        console.log(yourTurn);
        setInput();
        resetPos();
        showHide(true);
    });

    this.socket.on('playerDisconnect', function () {
        player = 0;
        opponent = 1;
        firstPlayer = true;
        yourTurn = true;
        console.log(yourTurn);
        setInput();
        resetPos();
    });

    this.socket.on('wait', function () {
        showHide(false);
    });

    this.socket.on('isPlayerB', function () {
        if (!firstPlayer) {
            player = 1;
            opponent = 0;
            yourTurn = false;
            console.log(yourTurn);
            setInput();
            resetPos();
            showHide(true);
        }
    });
}

function game_over()
{
    yourTurn = false;
    statusText.setText('Game Over');
    Phaser.Display.Align.In.Center(statusText, bg);
    statusText.visible = true;
    setInput();
}

function showHide(value)
{
    bg.visible = value;
    for (let c = 0; c < 8; c++)
    {
        pawns[player][c].visible = value;
        pawns[opponent][c].visible = value;
    }
    for (let c = 0; c < 2; c++)
    {
        rooks[player][c].visible = value;
        rooks[opponent][c].visible = value;
    }
    for (let c = 0; c < 2; c++)
    {
        knights[player][c].visible = value;
        knights[opponent][c].visible = value;
    }
    for (let c = 0; c < 2; c++)
    {
        bishops[player][c].visible = value;
        bishops[opponent][c].visible = value;
    }

    queens[player].visible = value;
    kings[player].visible = value;
    queens[opponent].visible = value;
    kings[opponent].visible = value;
}

function setInput()
{
    for (let c = 0; c < 8; c++)
    {
        pawns[player][c].input.enabled = yourTurn;
        pawns[opponent][c].input.enabled = false;
    }
    for (let c = 0; c < 2; c++)
    {
        rooks[player][c].input.enabled = yourTurn;
        rooks[opponent][c].input.enabled = false;
    }
    for (let c = 0; c < 2; c++)
    {
        knights[player][c].input.enabled = yourTurn;
        knights[opponent][c].input.enabled = false;
    }
    for (let c = 0; c < 2; c++)
    {
        bishops[player][c].input.enabled = yourTurn;
        bishops[opponent][c].input.enabled = false;
    }

    queens[player].input.enabled = yourTurn;
    kings[player].input.enabled = yourTurn;
    queens[opponent].input.enabled = false;
    kings[opponent].input.enabled = false;
}

function getSquare(dropZone)
{
    for (let c = 0; c < 8; c++) 
    {
        for (let i = 0; i < 8; i++)
        {
            if (dropZone.x == zones[c][i].x) {
                if (dropZone.y == zones[c][i].y)
                    return[7 - c, 7 - i];
            }
        }
    }
}

function getPeice(dropZone)
{
    for (let i = 0; i < 2; i++) {
        for (let c = 0; c < 8; c++)
        {
            console.log(dropZone.x, dropZone.y, pawns[i][c].x, pawns[i][c].y);
            if (dropZone.x == pawns[i][c].x && dropZone.y == pawns[i][c].y)
            {
                return pawns[i][c];
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == rooks[i][c].x && dropZone.y == rooks[i][c].y)
            {
                return rooks[i][c];
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == knights[i][c].x && dropZone.y == knights[i][c].y)
            {
                return knights[i][c];
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == bishops[i][c].x && dropZone.y == bishops[i][c].y)
            {
                return bishops[i][c];
            }
        }
    
        if (dropZone.x == queens[i].x && dropZone.y == queens[i].y)
        {
            return queens[i];
        }
        if (dropZone.x == kings[i].x && dropZone.y == kings[i].y)
        {
            return kings[i];
        }
    }
    return 0;
}

function checkSquare(dropZone)
{
    for (let i = 0; i < 2; i++) {
        for (let c = 0; c < 8; c++)
        {
            if (dropZone.x == pawns[i][c].x && dropZone.y == pawns[i][c].y)
            {
                pawns[i][c].x = PIT;
                pawns[i][c].y = PIT;
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == rooks[i][c].x && dropZone.y == rooks[i][c].y)
            {
                rooks[i][c].x = PIT;
                rooks[i][c].y = PIT;
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == knights[i][c].x && dropZone.y == knights[i][c].y)
            {
                knights[i][c].x = PIT;
                knights[i][c].y = PIT;
            }
        }
        for (let c = 0; c < 2; c++)
        {
            if (dropZone.x == bishops[i][c].x && dropZone.y == bishops[i][c].y)
            {
                bishops[i][c].x = PIT;
                bishops[i][c].y = PIT;
            }
        }
    
        if (dropZone.x == queens[i].x && dropZone.y == queens[i].y)
        {
            queens[i].x = PIT;
            queens[i].y = PIT;
        }
        if (dropZone.x == kings[i].x && dropZone.y == kings[i].y)
        {
            kings[i].x = PIT;
            kings[i].y = PIT;
            gameOver = true;
            game_over();
        }
    }
}

function resetPos()
{
    for (let c = 0; c < 8; c++) 
    {
        pawns[player][c].x = zones[c][6].x;
        pawns[player][c].y = zones[c][6].y;
    }

    for (let c = 0; c < 8; c++) 
    {
        pawns[opponent][c].x = zones[c][1].x;
        pawns[opponent][c].y = zones[c][1].y;
    }
    rooks[player][0].x = zones[0][7].x;
    rooks[player][0].y = zones[0][7].y;
    rooks[player][1].x = zones[7][7].x;
    rooks[player][1].y = zones[7][7].y;
    rooks[opponent][0].x = zones[7][0].x;
    rooks[opponent][0].y = zones[7][0].y;
    rooks[opponent][1].x = zones[0][0].x;
    rooks[opponent][1].y = zones[0][0].y;
    
    knights[player][0].x = zones[1][7].x;
    knights[player][0].y = zones[1][7].y;
    knights[player][1].x = zones[6][7].x;
    knights[player][1].y = zones[6][7].y;
    knights[opponent][0].x = zones[6][0].x;
    knights[opponent][0].y = zones[6][0].y;
    knights[opponent][1].x = zones[1][0].x;
    knights[opponent][1].y = zones[1][0].y;

    bishops[player][0].x = zones[2][7].x;
    bishops[player][0].y = zones[2][7].y;
    bishops[player][1].x = zones[5][7].x;
    bishops[player][1].y = zones[5][7].y;
    bishops[opponent][0].x = zones[5][0].x;
    bishops[opponent][0].y = zones[5][0].y;
    bishops[opponent][1].x = zones[2][0].x;
    bishops[opponent][1].y = zones[2][0].y;
    queens[player].x = zones[3][7].x;
    queens[player].y = zones[3][7].y;
    queens[opponent].x = zones[4][0].x;
    queens[opponent].y = zones[4][0].y;
    kings[player].x = zones[4][7].x;
    kings[player].y = zones[4][7].y;
    kings[opponent].x = zones[3][0].x;
    kings[opponent].y = zones[3][0].y;
}

function update ()
{
    // //do not update if client not ready
    // if (!ready) return;
    //const cam = this.cameras.main;
    //cam.rotation += 0.01;
    //rotate_sprites(-0.001);
}

function rotate_sprites(value)
{
    for (let c = 0; c < 8; c++) 
    {
        pawns[0][c].rotation += value;
        pawns[0][c].rotation += value;
    }

    for (let c = 0; c < 8; c++) 
    {
        pawns[1][c].rotation += value
        pawns[1][c].rotation += value
    }
    rooks[0][0].rotation += value
    rooks[0][0].rotation += value
    rooks[0][1].rotation += value
    rooks[0][1].rotation += value
    rooks[1][0].rotation += value
    rooks[1][0].rotation += value
    rooks[1][1].rotation += value
    rooks[1][1].rotation += value
    
    knights[0][0].rotation += value
    knights[0][0].rotation += value
    knights[0][1].rotation += value
    knights[0][1].rotation += value
    knights[1][0].rotation += value
    knights[1][0].rotation += value
    knights[1][1].rotation += value
    knights[1][1].rotation += value

    bishops[0][0].rotation += value
    bishops[0][0].rotation += value
    bishops[0][1].rotation += value
    bishops[0][1].rotation += value
    bishops[1][0].rotation += value
    bishops[1][0].rotation += value
    bishops[1][1].rotation += value
    bishops[1][1].rotation += value
    queens[0].rotation += value
    queens[0].rotation += value
    queens[1].rotation += value
    queens[1].rotation += value
    kings[0].rotation += value
    kings[0].rotation += value
    kings[1].rotation += value
    kings[1].rotation += value
}