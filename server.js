var express = require('express');
//const { isPromise } = require('util/types');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
let players = [];

app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    console.log('A user connected: ' + socket.id);
    players.push(socket.id);

    if (players.length === 1) {
        console.log("Player 1");
        io.emit('isPlayerA');
    }
    else if (players.length === 2) {
        console.log("Player 2");
        io.emit('isPlayerB');
    }
    else
    {
        console.log("Wait!");
        io.emit("wait");
    };

    socket.on('placePeice', function (oldZone, newZone, player) {
        console.log(newZone);
        io.emit('placePeice', oldZone, newZone, player);
    });

    socket.on('gameEnd', function () {
        console.log("Game over!");
        io.emit('gameOver');
    });

    socket.on('disconnect', function () {
        console.log('A user disconnected: ' + socket.id);
        players.pop();
        io.emit('playerDisconnect');
    });
});

server.listen(8081, function () {
  console.log(`Listening on ${server.address().port}`);
});